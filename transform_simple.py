"""
Acepta dos parámetros: 
- el nombre de un fichero de imagen en formato RGB
- el nombre de una función de transformación entre las siguientes: 
    * rotate_right, mirror, blur, greyscale. 

El programa deberá leer la imagen, aplicarle la transformación indicada, y producir un fichero 
con el nombre y extensión del fichero de entrada, pero con el sufijo _trans en el nombre. 
Por ejemplo, si el fichero original se llama fichero.png, el fichero de salida se llamará fichero_trans.png. 

Un ejemplo de ejecución será:
python transform_simple.py fichero.png rotate_right
"""

from pathlib import Path
from transforms import rotate_right, mirror, blur, grayscale
from images import read_img, write_img
import sys

def main():
    params = sys.argv[1:]

    avaliable_transform_fn = {
        "rotate_right": rotate_right,
        "mirror": mirror,
        "blur": blur,
        "grayscale": grayscale,
    }

    if len(params) != 2:
        print("[ERROR] Nº params: must be 2 as follow")
        print("transform_simple.py [img_path] [tranform_funtion]")
        print("Avaliable [tranform_funtion] are:", avaliable_transform_fn.keys())
        exit(0)

    image_path = Path(params[0])
    transform_fn = params[1]

    if not image_path.exists():
        print(f"[ERROR] image_path = {image_path.resolve()} no exists")
        exit(0)

    if not transform_fn in avaliable_transform_fn:
        print(f"[ERROR] transform_fn = {transform_fn} no exists")
        print("Avaliable [tranform_funtion] are:", avaliable_transform_fn.keys())
        exit(0)

    print(f"Apply [{transform_fn}] funtion to image {image_path}")

    # Leer la imagen
    img = read_img(str(image_path.resolve()))

    # Aplicar la función
    transform_img = avaliable_transform_fn[transform_fn](img)

    # Guardar la imagen resultado
    dst_path = image_path.parent / f"{image_path.stem}_trans{image_path.suffix}"
    write_img(transform_img, str(dst_path))
    print(f"[INFO] result image saved in {dst_path.resolve()}")

if __name__ == '__main__':
    main()