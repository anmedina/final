"""
Acepta dos o más parámetros: 
- el nombre de un fichero de imagen en formato RGB
- el nombre de una función de transformación

En caso de que la función admita parámetros, se incluirán a continuación. 

Función change_colors, serán seis números enteros, que serán los valores RGB a cambiar, 
        y los valores RGB por los que se cambiarán.
Función rotate_colors, será un número entero, el incremento que se aplicará a todos los colores. 
Función shift, serán dos números enteros, el número de pixels a desplazar horizontalmente 
        y el número de pixels a desplazar verticalmente.


El programa deberá leer la imagen, aplicarle la transformación indicada, y producir un fichero 
con el nombre y extensión del fichero de entrada, pero con el sufijo _trans en el nombre. 
Por ejemplo, si el fichero original se llama fichero.png, el fichero de salida se llamará fichero_trans.png. 

Un ejemplo de ejecución será:
python transform_args.py fichero.png change_colors 255 0 0 0 255 0
"""

from pathlib import Path
from transforms import change_colors, rotate_right, mirror, rotate_colors, blur, shift, crop, grayscale, filter
from images import read_img, write_img
import sys

def main():
    params = sys.argv[1:]

    # Los valores son una tupla con la función y el nº de parametros que se necesitan a mayores para cada funcion
    avaliable_transform_fn = {
        "change_colors": (change_colors, 6),
        "rotate_right": (rotate_right, 0),
        "mirror": (mirror, 0),
        "rotate_colors": (rotate_colors, 1),
        "blur": (blur, 0),
        "shift": (shift, 2),
        "crop": (crop, 0),
        "grayscale": (grayscale, 0),
        "filter": (filter, 3),
    }

    if len(params) <= 2:
        print("[ERROR] Nº params: must be great than 2 as follow")
        print("transform_simple.py [img_path] [tranform_funtion] [opt_params_for_tranform_funtion]")
        print("Avaliable [tranform_funtion] are:", avaliable_transform_fn.keys())
        print("Explanation about [opt_params_for_tranform_funtion]")
        print("[change_colors] fn needs 6 int numbers, which will be the RGB values to be changed, and the RGB values by which they will be changed.")
        print("[rotate_colors] fn needs 1 int number, the increment that will be applied to all colors.")
        print("[shift] fn needs 2 int numbers, the number of pixels to be displaced horizontally and the number of pixels to be displaced vertically.")
        print("[filter] fn needs 3 int numbers, the filter multiplier for each of the RGB components.")

        exit(0)

    image_path = Path(params[0])
    transform_fn = params[1]

    if not image_path.exists():
        print(f"[ERROR] image_path = {image_path.resolve()} no exists")
        exit(0)

    if not transform_fn in avaliable_transform_fn:
        print(f"[ERROR] transform_fn = {transform_fn} no exists")
        print("Avaliable [tranform_funtion] are:", avaliable_transform_fn.keys())
        exit(0)


    # Check needed params by transform_fn
    if len(params[2:]) != avaliable_transform_fn[transform_fn][1]:
        print(f"[ERROR] [opt_params_for_tranform_funtion]")
        print("Explanation about [opt_params_for_tranform_funtion]")
        print("[change_colors] fn needs 6 int numbers, which will be the RGB values to be changed, and the RGB values by which they will be changed.")
        print("[rotate_colors] fn needs 1 int number, the increment that will be applied to all colors.")
        print("[shift] fn needs 2 int numbers, the number of pixels to be displaced horizontally and the number of pixels to be displaced vertically.")
        print("[filter] fn needs 3 float numbers, the filter multiplier for each of the RGB components.")

        exit(0)

    print(f"Apply [{transform_fn}] funtion to image {image_path}")

    # Leer la imagen
    img = read_img(str(image_path.resolve()))

    # Aplicar la función
    if avaliable_transform_fn[transform_fn][1] == 0:
        transform_img = avaliable_transform_fn[transform_fn][0](img) # [rotate_right], [mirror], [blur], [greyscale]
    elif avaliable_transform_fn[transform_fn][1] == 1: # [rotate_colors] 
        transform_img = avaliable_transform_fn[transform_fn][0](img, int(params[2]))
    elif avaliable_transform_fn[transform_fn][1] == 2: # [shift] 
        transform_img = avaliable_transform_fn[transform_fn][0](img, int(params[2]), int(params[3]))
    elif avaliable_transform_fn[transform_fn][1] == 3: # [filter] 
        transform_img = avaliable_transform_fn[transform_fn][0](img, float(params[2]), float(params[3]), float(params[4]))
    elif avaliable_transform_fn[transform_fn][1] == 6: # [change_colors] 
        to_change = (int(params[2]), int(params[3]), int(params[4]))
        to_change_to = (int(params[5]), int(params[6]), int(params[7]))
        transform_img = avaliable_transform_fn[transform_fn][0](img, to_change, to_change_to)

    # Guardar la imagen resultado
    dst_path = image_path.parent / f"{image_path.stem}_trans{image_path.suffix}"
    write_img(transform_img, str(dst_path))
    print(f"[INFO] result image saved in {dst_path.resolve()}")


if __name__ == '__main__':
    main()