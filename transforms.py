"""
Funciones necesarias para tratar imágenes digitales
"""

from images import *


######################################### Auxiliar funtions #########################################

def _create_color_image(width: int, height: int, color: tuple[int, int, int]) -> list[list[tuple[int, int, int]]]:
    """
    Produce una imagen con todos los pixeles del color sugerido.

    Args:
        width: ancho de la imagen.
        height: alto de la imagen.
        color: color de la imagen.

    Return:
        image: imagen con todos los pixeles del color sugerido.
    """

    return_image = []

    for x in range(width):
        return_image.append([0] * height)
        for y in range(height):
            return_image[x][y] = color

    return return_image


def _copy_image(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    """
    Copia pixel a pixel una imagen en otra.

    Args:
        image: imagen original.

    Return:
        image: imagen copia.
    """
    width, height = size(image)

    raw_image = _create_color_image(width, height, (255, 255, 255))

    for x in range(width):
        for y in range(height):
            raw_image[x][y] = image[x][y]

    return raw_image


######################################### PRACTICE FUNTIONS #########################################
            
def change_colors(
    image: list[list[tuple[int, int, int]]],
    to_change: tuple[int, int, int],
    to_change_to: tuple[int, int, int]
) -> list[list[tuple[int, int, int]]]:
    """
    Produce una nueva imagen con los colores cambiados.

    Args:
        image: imagen original.
        to_change: color a cambiar.
        to_change_to: color por el que será cambiado.

    Return:
        image: imagen con los colores camibados.
    """
    
    return_image = _copy_image(image)
    width, height = size(image)

    for x in range(width):
        for y in range(height):
            color = image[x][y]
            if color == to_change:
                return_image[x][y] = to_change_to

    return return_image


def rotate_right(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    """
    Produce una nueva imagen girada 90 grados hacia la derecha.

    Args:
        image: imagen a girar.

    Return:
        image: imagen girada.
    """
    
    width, height = size(image)

    return_image = _create_color_image(width=height, height=width, color=(255, 255, 255))

    for x in range(width):
        for y in range(height):
            return_image[y][x] = image[x][height-(y+1)]

    return return_image


def mirror(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    """
    Produce una nueva imagen "espejada" según un eje vertical situado en la mitad de la imagen.

    Args:
        image: imagen a espejar.

    Return:
        image: imagen espejada.
    """

    return_image = _copy_image(image)

    width, height = size(image)
    for x in range(width):
        for y in range(height):
            return_image[x][y] = image[width-(x+1)][y]

    return return_image


def rotate_colors(image: list[list[tuple[int, int, int]]], increment: int) -> list[list[tuple[int, int, int]]]:
    """
    Produce una nueva imagen con los colores cambiados.
    Para cambiarlos, sumará a cada uno de los componentes RGB el número que se le indique como incremento, 
    teniendo en cuenta que si el valor llega a 255, tendrá que volver a empezar en 0 (esto es, se incrementará módulo 256).
    El incremento podrá ser también un valor negativo.

    Args:
        image: imagen original.
        increment: incremento a realizar a los valores RGB de cada pixel.
    Return:
        image: imagen espejada.
    """
    return_image = _copy_image(image)


    width, height = size(image)

    for x in range(width):
        for y in range(height):
            red, green, blue = image[x][y]

            nuevo_color = (red + increment) % 256, (green + increment) % 256, (blue + increment) % 256

            return_image[x][y] = nuevo_color

    return return_image


def blur(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    """
    Creará una nueva imagen donde el valor RGB de cada pixel será el valor medio de los valores RGB 
    de los pixels que tiene encima, debajo, a la derecha y a la izquierda (salvo que esté en los bordes,
    donde obviamente no se considerá el valor correspondiente: por ejemplo, si está arriba del todo, no se
    podrá usar el valor del pixel de encima, porque no lo hay). 

    Args:
        image: imagen a modificar.

    Return:
        image: imagen modificada.
    """
    
    return_image = _copy_image(image)

    width, height = size(image)

    for x in range(width):
        for y in range(height):
            # Coger los colores de: arriba, abajo, derecha y izquierda
            # Si no hay, se asigna cero
            up_color = image[x-1][y] if x > 0 else (0, 0, 0)
            down_color = image[x+1][y] if x+1 < width else (0, 0, 0)
            left_color = image[x][y-1] if y > 0 else (0, 0, 0)
            right_color = image[x][y+1] if y+1 < height else (0, 0, 0)

            # Blur: hay que saber por cuantas componentes dividir para hacer el promedio,
            # es decir, cuantas componentes son distintas de cero (contorno de la imagen)
            # por ejemplo, si está arriba del todo, no se podrá usar el valor del pixel de encima, porque no lo hay

            # Blur rojo
            red_colors = [up_color[0], down_color[0], left_color[0], right_color[0]]
            non_zero_red = sum(1 for x in red_colors if x != 0)
            red_blur = int(sum(red_colors) / non_zero_red) if non_zero_red > 0 else 0

            # Blur verde
            green_colors = [up_color[1], down_color[1], left_color[1], right_color[1]]
            non_zero_green = sum(1 for x in green_colors if x != 0)
            green_blur = int(sum(green_colors) / non_zero_green) if non_zero_green > 0 else 0

            # Blur azul
            blue_colors = [up_color[2], down_color[2], left_color[2], right_color[2]]
            non_zero_blue = sum(1 for x in blue_colors if x != 0)
            blue_blur = int(sum(blue_colors) / non_zero_blue) if non_zero_blue > 0 else 0

            return_image[x][y] = red_blur, green_blur, blue_blur
    
    return return_image


def shift(image: list[list[tuple[int, int, int]]], horizontal: int, vertical: int) -> list[list[tuple[int, int, int]]]:
    """
    Desplazará la imagen el número de pixels que se indique, en el eje horizontal o vertical, según el parámetro que se indique.

    Args:
        image: imagen a modificar.
        horizontal: la cantidad de pixels a desplazar horizontalmente (hacia la derecha si es positivo, hacia la izquierda si es negativo).
        vertical: la cantidad de pixels a desplazar verticalmente (hacia arriba si es positivo, hacia abajo si es negativo).
    Return:
        image: imagen modificada.
    """

    return_image = _copy_image(image)
    width, height = size(image)

    # Desplazar horizontalmente
    if horizontal != 0:
        for fila in range(width):
            return_image[fila] = return_image[fila][-horizontal:] + return_image[fila][:-horizontal]

    # Desplazar verticalmente
    if vertical != 0:
        return_image = return_image[-vertical:] + return_image[:-vertical]

    return return_image


def crop(image: list[list[tuple[int, int, int]]], x: int, y: int, width: int, height: int) -> list[list[tuple[int, int, int]]]:
    """
    Creará una nueva imagen que contendrá solo los pixels que se encuentren dentro de un rectángulo que se especifique.

    Args:
        image: imagen a recortar.
        x: coordenada x de la esquina superior izquierda.
        y: coordenada y de la esquina superior izquierda.
        width: ancho de la imagen deseada.
        height: alto de la imagen deseada.
    Return:
        image: imagen modificada.
    """

    width, height = size(image)

    return_image = [row[x:x+width] for row in image[y:y+height]]

    return return_image


def grayscale(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    """
    Creará una nueva imagen que contendrá la imagen original, pero en escala de grises.
    Para convertir un píxel RGB a gris, basta con calcular la media de los tres valores de la tupla RGB, 
    y asignar ese valor a los tres valores de la tupla RGB del pixel equivalente en la imagen resultante.

    Args:
        image: imagen a convertir.
    Return:
        image: imagen modificada.
    """
    
    return_image = _copy_image(image)
    width, height = size(image)

    for x in range(width):
        for y in range(height):
            red, green, blue = image[x][y]

            gray_color = int((red + green + blue) / 3)

            return_image[x][y] = (gray_color, gray_color, gray_color)

    return return_image


def filter(
    image: list[list[tuple[int, int, int]]],
    r: float,
    g: float,
    b: float
) -> list[list[tuple[int, int, int]]]:
    """
    Creará una nueva imagen que contendrá la imagen original, pero con un filtro aplicado 
    a todos sus pixels. El filtro se especificará como un multiplicador para cada valor RGB. 
    Cada pixel de la imagen resultante tendrá una tupla RGB igual a la de la imagen original,
    con cada componente RGB multiplicado por su multiplicador correspondiente, teniendo en cuenta
    que si el resultado es mayor que el valor máximo posible para el componente, el valor será ese
    máximo posible (normalmente, 255). 

    Args:
        image: imagen a convertir.
        r: multiplicador de filtro para el color rojo.
        g: multiplicador de filtro para el color verde.
        b:  multiplicador de filtro para el color azul.
    Return:
        image: imagen modificada.
    """

    return_image = _copy_image(image)
    width, height = size(image)

    for x in range(width):
        for y in range(height):
            red, green, blue = image[x][y]

            red_filter = min(int(red * r), 255)
            green_filter = min(int(green * r), 255)
            blue_filter = min(int(blue * r), 255)

            return_image[x][y] = (red_filter, green_filter, blue_filter)

    return return_image

