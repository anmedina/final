"""
Como transform_args.py pero admitiendo varios nombres de función transformadora, 
si es caso seguidos por sus parámetros. El primer argumento será la imagen a 
transformar, el siguiente la primera función de transformación, seguida si es 
caso por sus argumentos, seguidos de la segunda función de transformación, y así
sucesivamente. El programa deberá leer la imagen, aplicarle las transformaciones
indicadas, y producir la imagen resultante, que tendrá el mismo nombre que se
indica en los programas anteriores. Dos ejemplos de ejecución serán:

python3 transform_multi.py fichero.png rotate_colors 90 mirror blur
python3 transform_multi.py fichero.png rotate_colors 90 change_colors 255 0 0 0 255 0 shift -20 20


"""

from pathlib import Path
from transforms import change_colors, rotate_right, mirror, rotate_colors, blur, shift, crop, grayscale, filter
from images import read_img, write_img
import sys

# Los valores son una tupla con la función y el nº de parametros que se necesitan a 
# mayores para cada funcion
AVALIABLE_TRANSFORM_FN = {
    "change_colors": (change_colors, 6),
    "rotate_right": (rotate_right, 0),
    "mirror": (mirror, 0),
    "rotate_colors": (rotate_colors, 1),
    "blur": (blur, 0),
    "shift": (shift, 2),
    "crop": (crop, 0),
    "grayscale": (grayscale, 0),
    "filter": (filter, 3),
}


def main():
    params = sys.argv[1:]

    if len(params) <= 2:
        print("[ERROR] Nº params: must be great than 2 as follow")
        print("transform_simple.py [img_path] [tranform_funtion] [opt_params_for_tranform_funtion]")
        print("Avaliable [tranform_funtion] are:", AVALIABLE_TRANSFORM_FN.keys())
        print("Explanation about [opt_params_for_tranform_funtion]")
        print("[change_colors] fn needs 6 int numbers, which will be the RGB values to be changed, and the RGB values by which they will be changed.")
        print("[rotate_colors] fn needs 1 int number, the increment that will be applied to all colors.")
        print("[shift] fn needs 2 int numbers, the number of pixels to be displaced horizontally and the number of pixels to be displaced vertically.")
        print("[filter] fn needs 3 int numbers, the filter multiplier for each of the RGB components.")

        exit(0)

    image_path = Path(params.pop(0))
    
    # Leer la imagen
    img = read_img(str(image_path.resolve()))

    while len(params) > 0:
        transform_fn = params.pop(0)

        if transform_fn in AVALIABLE_TRANSFORM_FN.keys():
            needed_pararams = AVALIABLE_TRANSFORM_FN[transform_fn][1]

            passed_params = params[:needed_pararams]

            # Check nº de parametros no es correcto:
            if len(passed_params) != needed_pararams:
                print(f"[ERROR] [opt_params_for_tranform_funtion]")
                print("Explanation about [opt_params_for_tranform_funtion]")
                print("[change_colors] fn needs 6 int numbers, which will be the RGB values to be changed, and the RGB values by which they will be changed.")
                print("[rotate_colors] fn needs 1 int number, the increment that will be applied to all colors.")
                print("[shift] fn needs 2 int numbers, the number of pixels to be displaced horizontally and the number of pixels to be displaced vertically.")
                print("[filter] fn needs 3 int numbers, the filter multiplier for each of the RGB components.")

                exit(0)

            # Aplicar la función
            print(f"Apply [{transform_fn}] funtion to image {image_path}")

            if AVALIABLE_TRANSFORM_FN[transform_fn][1] == 0:
                img = AVALIABLE_TRANSFORM_FN[transform_fn][0](img) # [rotate_right], [mirror], [blur], [greyscale]
            elif AVALIABLE_TRANSFORM_FN[transform_fn][1] == 1: # [rotate_colors] 
                img = AVALIABLE_TRANSFORM_FN[transform_fn][0](img, int(passed_params[0]))
            elif AVALIABLE_TRANSFORM_FN[transform_fn][1] == 2: # [shift] 
                img = AVALIABLE_TRANSFORM_FN[transform_fn][0](img, int(passed_params[0]), int(passed_params[1]))
            elif AVALIABLE_TRANSFORM_FN[transform_fn][1] == 3: # [filter] 
                img = AVALIABLE_TRANSFORM_FN[transform_fn][0](img, float(passed_params[0]), float(passed_params[1]), float(passed_params[2]))
            elif AVALIABLE_TRANSFORM_FN[transform_fn][1] == 6: # [change_colors] 
                to_change = (int(passed_params[0]), int(passed_params[1]), int(passed_params[2]))
                to_change_to = (int(passed_params[3]), int(passed_params[4]), int(passed_params[5]))
                img = AVALIABLE_TRANSFORM_FN[transform_fn][0](img, to_change, to_change_to)


            # Hacer pop de tantos elementos como parametros
            for _ in range(len(passed_params)):
                params.pop(0)
        else:
            print(f"[ERROR] transform_fn = {transform_fn} no exists")
            print("Avaliable [tranform_funtion] are:", AVALIABLE_TRANSFORM_FN.keys())
            exit(0)
            

    # Guardar la imagen resultado
    dst_path = image_path.parent / f"{image_path.stem}_trans{image_path.suffix}"
    write_img(img, str(dst_path))
    print(f"[INFO] result image saved in {dst_path.resolve()}")


if __name__ == '__main__':
    main()